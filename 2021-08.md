
eugyppius: a plague chronicle  
**The Disappearance of Influenza**  
https://eugyppius.substack.com/p/the-disappearance-of-influenza

---

TheHill  
**Southwest, American, Delta break with United, won't mandate vaccine for workers**  
https://thehill.com/policy/transportation/aviation/567308-southwest-american-delta-break-with-united-over-vaccine  

---

The New York Times  
**Israel, Once the Model for Beating Covid, Faces New Surge of Infections**  
https://www.nytimes.com/2021/08/18/world/middleeast/israel-virus-infections-booster.html  
[Alt Link](https://www.nytimesn7cgmftshazwhfgzm37qxb44r64ytbb2dj3x62d2lljsciiyd.onion/2021/08/18/world/middleeast/israel-virus-infections-booster.html)

---

